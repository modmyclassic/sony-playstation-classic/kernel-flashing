Preparing and flashing a kernel image on the PlayStation Classic
================================================================

This document and associated files will help you prepare a kernel image and
instruct how to flash it to your PlayStation Classic. It assumes that you have
already built a kernel image successfully.

Preparing a kernel image
------------------------
The PSC uses a LK-based bootloader that reads kernel images packed in the U-Boot
FIT format. In addition, the only compression format that the bootloader
supports is LZ4. Because of this, you need a few tools to successfully generate
an image that the bootloader can load. You will need the following:

- U-Boot tools: In particular, `mkimage`. To avoid installing a copy of U-Boot
  tools that doesn't support the required options, I suggest cloning the [U-Boot
  repository](http://git.denx.de/u-boot.git), picking any config, then doing
  `make tools`.
- Compatible version of the `lz4` command: The LZ4 decompression code inside the
  bootloader is based on LZ4 r90. Newer versions do not generate the same file
  format, and even if they could, may not decode properly. In addition, there
  is a custom footer that indicates the decompressed data length in the compressed
  file, which no stock lz4 command writes. As a convenience, a copy of the command
  compatible with the bootloader is provided.
- Fastboot: If you're flashing through fastboot, you need to have the program
  installed to send the images to your device. It is available in the Android SDK
  and sometimes as an individual package from your distro's package manager.

In addition to tools, you also need a FIT source file. And to ensure consistency,
we're using the device tree that came with the PSC instead of the version included
in the kernel archive, since they may be slightly out of date and don't match up
with the actual hardware. Both of those are provided.

1. Compress the kernel image.
   ```
   lz4 -c1 Image Image.lz4
   ```
2. Check `kernel_ftd.its` and update paths as necessary. This file is the FIT
   source file, and defines the structures for the FIT image. Use the right
   file for the kernel architecture that you compiled.
3. Compile the FIT image.
   ```
   mkimage -f kernel_ftd.its kernel.itb
   ```

Backup your existing kernel image
---------------------------------
Before flashing a new kernel, it is prudent to backup the original kernel
installed. You should backup both the kernel image and the Trusted Execution
Environment image.

Adapt the following:
```
dd if=/dev/disk/by-partlabel/BOOTIMG1 of=/media/orig_kernel.img bs=512K
dd if=/dev/disk/by-partlabel/TEE1 of=/media/orig_tee.img bs=512K
```

Entering fastboot
-----------------
We will be using fastboot to flash our new image. To enter fastboot, remove the
bottom panel of your PlayStation Classic. Above the letters "LM-11", bridge the
two large pads. Keep these pads bridged while you connect the USB cable to your
computer. Hold the bridge for about five seconds, and you can release after the
fastboot device replaces the MediaTek download mode device on your computer.

Preparing and installing the matching TEE image
-----------------------------------------------
If you're switching from a 32-bit kernel to 64-bit or vice versa, you will need
to update your TEE image with one that puts the system in the right bitness. A
patch is included for converting 32-bit TEE to 64-bit TEE (and vice versa), and
you apply that against your origin TEE image with `bspatch` before flashing it. 
It is strongly advised to use your original TEE image if you're going from 64-bit
to 32-bit kernel.

```
bspatch TEE_32.img TEE_64.img TEE_32_TO_64.bspatch
sha256sum TEE_64.img
2818a64c1e7750e35fe8a6a65f66500c9fc155f4d98597fd02956651e60160ef  TEE_64.img
```

```
bspatch TEE_64.img TEE_32.img TEE_64_TO_32.bspatch
sha256sum TEE_32.img
956f0300abef8ae6a599e8d9240b0b9b4b9f3e0d704ccdfba9ec9454624784bd  TEE_32.img
```


To flash the TEE image, run fastboot as follows:
```
fastboot erase TEE1
fastboot flash TEE1 tee.img
```

Flashing the kernel
-------------------
Enter fastboot mode, and do the following:
```
fastboot erase BOOTIMG1
fastboot flash BOOTIMG1 kernel.itb
fastboot reboot
```

Your device will reboot and attempt to boot the new kernel. Monitoring the boot
through UART is suggested when you're booting the new kernel for the first time.

Error modes
-----------
- If you get `decodes failure` from the bootloader, you are missing the decompressed
  length of the image at the end of your lz4 archive.
- If you get `data corrupt` from the bootloader, you are using the wrong version
  of the lz4 algorithm.
- If the bootloader crashes, it means you're missing some entries in your FIT
  image. Double check your FIT source file.
- If the bootloader jumps to TEE, and BL31 exits to the normal world, but nothing
  happens after that, you are probably using the wrong TEE for your kernel's
  bitness. Check the output and make sure the line that says `Kernel is` matches
  with the bitness that you compiled.
